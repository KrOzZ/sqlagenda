package com.krozz.agendasql;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.krozz.agendasql.database.AgendaContactos;
import com.krozz.agendasql.database.AgendaDBHelper;
import com.krozz.agendasql.database.Contacto;

public class MainActivity extends AppCompatActivity {


    private Button btnbuscar;
    private AgendaContactos db;
    private EditText txtNombre;
    private EditText txtTel1;
    private EditText txtTel2;
    private EditText txtDomicilio;
    private EditText txtNotas;
    private CheckBox cbFavorito;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnLista;
    private Button btnSalir;
    private Contacto saveContac= null;
    private long id;
    private boolean isEdit = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtNombre = findViewById(R.id.txtNombre);
        txtTel1 = findViewById(R.id.txtTel1);
        txtTel2 = findViewById(R.id.txtTel2);
        txtDomicilio = findViewById(R.id.txtDomicilio);
        txtNotas = findViewById(R.id.txtNotas);
        cbFavorito = findViewById(R.id.cbFavorito);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnLista = findViewById(R.id.btnLista);
        btnSalir = findViewById(R.id.btnSalir);


        btnGuardar.setOnClickListener(this.btnAgregarAction());

        this.btnSalir.setOnClickListener(this.btnSalirAction());
        //btnbuscar.setOnClickListener(this.btnBuscar());
        btnLista.setOnClickListener(this.btnListarAction());
        this.btnLimpiar.setOnClickListener(this.btnLimpiarAction());


    }

    private View.OnClickListener btnSalirAction(){
        return  new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };
    }

    private  View.OnClickListener btnLimpiarAction(){
        return  new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        };
    }
    private View.OnClickListener btnListarAction(){
        return  new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                startActivityForResult(intent,0);
            }
        };
    }
    private View.OnClickListener btnAgregarAction(){
        return  new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDataBase();
                if(validar()){
                    Contacto contacto = new Contacto();
                    contacto.setNombre((getText(txtNombre)));
                    contacto.setDireccion((getText(txtDomicilio)));
                    contacto.setTelefono((getText(txtTel1)));
                    contacto.setTelefono2((getText(txtTel2)));
                    contacto.setNotss(getText(txtNotas));
                    contacto.setFavorito(cbFavorito.isChecked());
                    if(!isEdit) {
                        if (db.insert(contacto) != -1)
                            Toast.makeText(MainActivity.this, "Se Agrego Correctamente", Toast.LENGTH_LONG).show();

                        else {
                            Toast.makeText(MainActivity.this, "Error Al Agregar", Toast.LENGTH_LONG).show();
                            limpiar();
                        }
                    }
                    else{
                        contacto.setId(id);
                        db.update(contacto);
                        Toast.makeText(MainActivity.this,"Se Actualizo correctamente",Toast.LENGTH_LONG).show();
                    }
                    limpiar();
                }
                else {
                    Toast.makeText(MainActivity.this,"Error Datos Faltantes",Toast.LENGTH_LONG).show();
                 }
                db.close();
            }
        };
    }

    /*private View.OnClickListener btnBuscar(){
        return new View.OnClickListener(){
            @Override
            public  void onClick (View v) {
                if (txtId.getText().toString().matches("")) {
                    lblNombre.setText("Campo Vacio");
                }
                else {
                    db= new AgendaContactos(MainActivity.this);
                    db.openDataBase();
                    Contacto contacto = new Contacto();
                    long id = Long.parseLong(txtId.getText().toString());
                    contacto = db.getContacto(id);
                    if (contacto == null) {
                        lblNombre.setText("Nose Encontro");
                    } else {
                        lblNombre.setText(contacto.getNombre());
                    }
                    db.close();
                }
            }
        };


    }*/

    private void limpiar(){
        txtDomicilio.setText("");
        txtTel1.setText("");
        txtTel2.setText("");
        txtNombre.setText("");
        txtNotas.setText("");
        cbFavorito.setChecked(false);
        isEdit=false;
        saveContac=null;
        id=0;
    }

    private boolean valCampo(EditText txt){
        return  txt.getText().toString().matches("");
    }

    private boolean validar(){
        if(valCampo(txtNombre)||valCampo(txtDomicilio)||valCampo(txtTel1)||valCampo(txtTel2)||valCampo(txtNotas)){
            return  false;
        }
        else
            return true;
    }
    private  String getText(EditText txt){
        return txt.getText().toString();
    }

    @Override
    protected  void onActivityResult (int requestCode,int resultCode,@Nullable Intent data){
        if (Activity.RESULT_OK == resultCode) {
            Contacto contacto=(Contacto)data.getSerializableExtra("contacto");
            saveContac=contacto;
            id= saveContac.getId();
            txtNombre.setText(saveContac.getNombre());
            txtDomicilio.setText(saveContac.getDireccion());
            txtTel1.setText(saveContac.getTelefono());
            txtTel2.setText(saveContac.getTelefono2());
            txtNotas.setText(saveContac.getNotss());
            cbFavorito.setChecked(saveContac.getFavorito());
            isEdit=true;
        }
        else{limpiar();}
        super.onActivityResult(requestCode,resultCode,data);
    }
}