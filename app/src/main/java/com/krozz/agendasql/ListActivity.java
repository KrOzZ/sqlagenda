package com.krozz.agendasql;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.krozz.agendasql.database.AgendaContactos;
import com.krozz.agendasql.database.Contacto;

import java.util.ArrayList;

public class ListActivity extends android.app.ListActivity {
    private AgendaContactos agendaContactos;
    private Button btnNuevo;
    private ArrayList<Contacto> contactos;
    private ContactosAdapter adapter;
    private ListView listContactos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        this.btnNuevo.setOnClickListener(this.btnNuevoAction());
        this.agendaContactos = new AgendaContactos(this);
        loadContactos();
        adapter = new ContactosAdapter(this, R.id.lbNombre, R.layout.layout_contacto, contactos);


        listContactos = findViewById(R.id.ListaContactos);
        listContactos.setAdapter(adapter);

    }
    private View.OnClickListener btnNuevoAction(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };
    }
    private void loadContactos()
    {
        agendaContactos.openDataBase();
        contactos = agendaContactos.allContacto();
        agendaContactos.close();
    }

    @Override
    public void setListAdapter(ListAdapter adapter) {
        super.setListAdapter(adapter);
    }

    class ContactosAdapter extends ArrayAdapter<Contacto> {
        private Context context;
        private int textViewResourceID;
        private ArrayList<Contacto> contactos;
        private LayoutInflater inflater;

        public ContactosAdapter(Context context, @IdRes int idTextView, @LayoutRes int  layoutIdResource, ArrayList<Contacto> items){
            super(context, idTextView,items);
            this.context = context;
            this.textViewResourceID = layoutIdResource;
            this.contactos = items;
            this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View view = this.inflater.inflate(this.textViewResourceID, parent, false);
            TextView lbNombre = view.findViewById(R.id.lbNombre);
            TextView lbTelefono = view.findViewById(R.id.lbTelefono);
            Button btnEditar = view.findViewById(R.id.btnModificar);
            Button btnEliminar = view.findViewById(R.id.btnEliminar);

            lbNombre.setTextColor(contactos.get(position).getFavorito() ? Color.BLUE : Color.BLACK);
            lbTelefono.setTextColor(contactos.get(position).getFavorito() ? Color.BLUE : Color.BLACK);

            lbNombre.setText(contactos.get(position).getNombre());
            lbTelefono.setText(contactos.get(position).getTelefono());
            btnEliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    agendaContactos.openDataBase();
                    agendaContactos.delete(contactos.get(position).getId());
                    agendaContactos.close();
                    contactos.remove(position);

                    notifyDataSetChanged();
                }
            });

            btnEditar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto", contactos.get(position));
                    Intent intent = new Intent();
                    intent.putExtras(bundle);
                    setResult(Activity.RESULT_OK);
                    finish();
                }
            });

            return view;
        }
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            return getView(position, convertView, parent);
        }



    }
}