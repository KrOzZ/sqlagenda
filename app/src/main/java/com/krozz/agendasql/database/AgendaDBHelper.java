package com.krozz.agendasql.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AgendaDBHelper extends SQLiteOpenHelper {


    private static final int DB_VERSION = 3;
    private static final String NAME_db = "agenda.db";
    private static String SQL_DELETE_CONTACTO = "DROP TABLE IF EXISTS " + DefinirTabla.TABLE_CONTACTO;

    public AgendaDBHelper(Context context)
    {

        super(context, NAME_db, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(this.createTable());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_CONTACTO);
        onCreate(db);

    }

    private String createTable()
    {
        SQLiteTabla tableContacto = new SQLiteTabla(DefinirTabla.TABLE_CONTACTO, DefinirTabla.ID);
        tableContacto.addColumn(DefinirTabla.NOMBRE, tableContacto.TYPE_TEXT);
        tableContacto.addColumn(DefinirTabla.TELEFONO1, tableContacto.TYPE_TEXT);
        tableContacto.addColumn(DefinirTabla.TELEFONO2, tableContacto.TYPE_TEXT);
        tableContacto.addColumn(DefinirTabla.DIRECCION, tableContacto.TYPE_TEXT);
        tableContacto.addColumn(DefinirTabla.FAVORITO, tableContacto.TYPE_INTEGER);
        tableContacto.addColumn(DefinirTabla.NOTAS, tableContacto.TYPE_TEXT);
        return tableContacto.getQuery();
    }
}
