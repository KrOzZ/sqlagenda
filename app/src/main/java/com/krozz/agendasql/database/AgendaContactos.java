package com.krozz.agendasql.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.constraintlayout.solver.widgets.Helper;

import java.net.ContentHandler;
import java.util.ArrayList;

import javax.sql.StatementEvent;

public class AgendaContactos {
    private Context context;
    private AgendaDBHelper agendaDBHelper;
    private SQLiteDatabase db;

    private String []  columnToRead = new String[]{
            DefinirTabla.ID,
            DefinirTabla.NOMBRE,
            DefinirTabla.TELEFONO1,
            DefinirTabla.TELEFONO2,
            DefinirTabla.DIRECCION,
            DefinirTabla.NOTAS,
            DefinirTabla.FAVORITO
    };

    public AgendaContactos(Context context) {
        this.context=context;
        agendaDBHelper= new AgendaDBHelper(this.context);

    }
    public void openDataBase(){
        db= agendaDBHelper.getWritableDatabase();
    }
    public long insert (Contacto c){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.NOMBRE,c.getNombre());
        values.put(DefinirTabla.TELEFONO1,c.getTelefono());
        values.put(DefinirTabla.TELEFONO2,c.getTelefono2());
        values.put(DefinirTabla.DIRECCION,c.getDireccion());
        values.put(DefinirTabla.NOTAS,c.getNotss());
        values.put(DefinirTabla.FAVORITO,c.getFavorito());
        return db.insert(DefinirTabla.TABLE_CONTACTO,null,values);
    }
    public long update (Contacto c){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.NOMBRE,c.getNombre());
        values.put(DefinirTabla.DIRECCION,c.getDireccion());
        values.put(DefinirTabla.TELEFONO1,c.getTelefono());
        values.put(DefinirTabla.TELEFONO2,c.getTelefono2());
        values.put(DefinirTabla.NOTAS,c.getNotss());
        values.put(DefinirTabla.FAVORITO,c.getFavorito());
        String criterio = DefinirTabla.ID+"="+c.getId();
        return db.update(DefinirTabla.TABLE_CONTACTO,values,criterio,null);
    }

    public int delete (long id){
        String criterio= DefinirTabla.ID+"="+id;

        return  db.delete(DefinirTabla.TABLE_CONTACTO,criterio,null);
    }
    public Contacto readContacto(Cursor cursor){
        Contacto c= new Contacto();
        c.setId(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setTelefono(cursor.getString(2));
        c.setTelefono2(cursor.getString(3));
        c.setDireccion(cursor.getString(4));
        c.setNotss(cursor.getString(5));
        c.setFavorito(cursor.getInt(6)==1);
        return c;
    }
    public Contacto getContacto(long id){
        Contacto contacto = null;
        SQLiteDatabase db= agendaDBHelper.getReadableDatabase();
        Cursor cursor = db.query(DefinirTabla.TABLE_CONTACTO,columnToRead,DefinirTabla.ID+"=?",new String[]{String.valueOf(id)},null,null,null);
        if(cursor.moveToFirst()){
            contacto = readContacto(cursor);
        }

        cursor.close();
        return  contacto;
    }

    public ArrayList<Contacto> allContacto(){
        ArrayList<Contacto> contactos = new ArrayList<Contacto>();
        Cursor cursor= db.query(DefinirTabla.TABLE_CONTACTO,columnToRead,null,null,null,null,null);
        if(cursor!=null){
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                contactos.add(readContacto(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return contactos;
    }

    public void close(){
        db.close();
    }
}
